using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using mvc_postgresql_demo.DAO;
using mvc_postgresql_demo.Models;

namespace mvc_postgresql_demo.ViewComponents
{
    public class PostViewComponent : ViewComponent
    {
        private IDAO<Post> _postDAO;

        public PostViewComponent(IDAO<Post> postDAO)
        {
            this._postDAO = postDAO;
        }
        public async Task<IViewComponentResult> InvokeAsync(int blogId)
        {
            // ViewData["blogId"] = blogId;
            // var posts = this._postDAO.GetMany(p => p.BlogId == blogId);
            // return await Task.Run(() => View(posts));
            return null;
        }
    }
}