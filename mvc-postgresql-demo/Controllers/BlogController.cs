using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using mvc_postgresql_demo.DAO;
using mvc_postgresql_demo.Models;
using mvc_postgresql_demo.Setting;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using mvc_postgresql_demo.ViewModels;
using PagedList.Core;

namespace mvc_postgresql_demo.Controllers
{
    public class BlogController : Controller
    {
        private IDAO<Blog> _blogDAO;
        private AppSettings _appSettings;
        // thêm Microsoft.Extensions.Logging namespace mới có
        private readonly ILogger<BlogController> _logger;

        //them
        public BlogController(IOptions<AppSettings> appSettings, ILogger<BlogController> logger, IDAO<Blog> blogDAO)
        {
            this._blogDAO = blogDAO;
            // this._appSettings = appSettings.Value;
            //them
            this._logger = logger;
        }

        public IActionResult Index(string sortOrder, string searchString, int? page)
        {
            ViewData["CurrentSortOrder"] = sortOrder;
            ViewData["BlogIdSort"] = String.IsNullOrEmpty(sortOrder) ? "BlogId_desc" : "";
            ViewData["UrlSort"] = sortOrder == "Url" ? "Url_desc" : "Url";
            if (searchString != null)
            {
                page = 1;
            }

            page = page == null || page <= 0 ? 1 : page;
            var pageSize = 10;
            var blogViewModel = new BlogViewModel();
            var blogs = this._blogDAO.GetMany(page.Value, pageSize, sortOrder);
            blogViewModel.Blogs = new StaticPagedList<Blog>(blogs, page.Value, pageSize, this._blogDAO.Count());
            // ViewData["ExampleConfig"] = this._appSettings.ExampleKey;
            this._logger.LogInformation("Index page say hello", new object[0]);
            return View(blogViewModel);
        }

        public IActionResult Detail(int id)
        {
            var blog = this._blogDAO.GetOne(b => b.BlogId == id);
            ViewData["Title"] = "Blog number " + id;
            return View(blog);
        }
    }
}