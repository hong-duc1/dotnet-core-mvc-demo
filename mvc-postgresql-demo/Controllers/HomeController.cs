﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace mvc_postgresql_demo.Controllers
{

    public class HomeController : Controller
    {
        // [Authorize(Policy = "AdminOnly")]
        public IActionResult Index()
        {
            return View();
        }

        // [Authorize(Roles = "SuperAdmin")]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View();
        }
    }
}
