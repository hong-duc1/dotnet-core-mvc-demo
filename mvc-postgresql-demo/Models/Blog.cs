using System.Collections.Generic;
namespace mvc_postgresql_demo.Models
{
    public class Blog
    {
        public int BlogId { get; set; }
        public string Url { get; set; }

        public List<Post> Posts { get; set; }
    }
}