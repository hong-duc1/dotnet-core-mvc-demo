using PagedList.Core;
using mvc_postgresql_demo.Models;

namespace mvc_postgresql_demo.ViewModels
{
    public class BlogViewModel
    {
        public IPagedList<Blog> Blogs { get; set; }
    }

}