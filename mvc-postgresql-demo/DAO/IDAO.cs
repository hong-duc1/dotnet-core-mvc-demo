using System.Collections.Generic;
using System;

namespace mvc_postgresql_demo.DAO
{
    public interface IDAO<T>
    {
        bool Add(T model);
        bool Remove(T model);
        bool Update(T model);
        T GetOne(Predicate<T> predicate);
        List<T> GetMany(int pageNumber, int pageSize, string sortOrder = null, Predicate<T> predicate = null);
        int Count();
    }
}