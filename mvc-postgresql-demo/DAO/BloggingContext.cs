using Microsoft.EntityFrameworkCore;
using mvc_postgresql_demo.Models;

namespace mvc_postgresql_demo.DAO
{
    public class BloggingContext : DbContext
    {
        public DbSet<Blog> Blogs { get; set; }
        public DbSet<Post> Posts { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder dbo)
        {
            dbo.UseNpgsql("Host=localhost;Port=5432;Database=Blogging;Username=hongduc;Password=123456789");
        }

    }
}