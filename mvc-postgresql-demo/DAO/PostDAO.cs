using System;
using System.Collections.Generic;
using mvc_postgresql_demo.Models;
using System.Linq;

namespace mvc_postgresql_demo.DAO
{
    public class PostDAO : IDAO<Post>
    {
        private BloggingContext context;
        public bool Add(Post model)
        {
            throw new NotImplementedException();
        }

        public int Count()
        {
            throw new NotImplementedException();
        }

        public List<Post> GetMany(Predicate<Post> predicate = null)
        {
            using (this.context = new BloggingContext())
            {
                if (predicate != null)
                {
                    return this.context.Posts.Where(p => predicate.Invoke(p)).ToList();
                }
                else
                {
                    return this.context.Posts.ToList();
                }
            }
        }

        public List<Post> GetMany(int pageNumber, int pageSize, string sortOrder, Predicate<Post> predicate = null)
        {
            throw new NotImplementedException();
        }

        public Post GetOne(Predicate<Post> predicate)
        {
            throw new NotImplementedException();
        }

        public bool Remove(Post model)
        {
            throw new NotImplementedException();
        }

        public bool Update(Post model)
        {
            throw new NotImplementedException();
        }
    }
}