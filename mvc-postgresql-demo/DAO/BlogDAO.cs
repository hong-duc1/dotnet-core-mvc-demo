using System;
using System.Collections.Generic;
using mvc_postgresql_demo.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace mvc_postgresql_demo.DAO
{
    public class BlogDAO : IDAO<Blog>
    {
        private BloggingContext context;

        public bool Add(Blog model)
        {
            using (context = new BloggingContext())
            {
                context.Add(model);
                var count = context.SaveChanges();
                return count > 0;
            }
        }

        public Blog GetOne(Predicate<Blog> predicate)
        {
            using (context = new BloggingContext())
            {
                var blog = context.Blogs
                     .Single(b => predicate.Invoke(b));
                return blog;
            }
        }

        public bool Remove(Blog model)
        {
            throw new NotImplementedException();
        }

        public bool Update(Blog model)
        {
            throw new NotImplementedException();
        }

        public List<Blog> GetMany(int pageNumber,
                int pageSize,
                string sortOrder = null,
                Predicate<Blog> predicate = null)
        {
            using (context = new BloggingContext())
            {
                var blogs = from blog in context.Blogs select blog;
                if (predicate != null)
                {
                    blogs = context.Blogs.Where(b => predicate.Invoke(b));
                }

                if (String.IsNullOrEmpty(sortOrder))
                {
                    sortOrder = "BlogId";
                }

                bool desending = false;
                if (sortOrder.EndsWith("_desc"))
                {
                    sortOrder = sortOrder.Substring(0, sortOrder.Length - 5);
                    desending = true;
                }

                if (desending)
                {
                    blogs = blogs.OrderByDescending(b => EF.Property<object>(b, sortOrder));
                }
                else
                {
                    blogs = blogs.OrderBy(b => EF.Property<object>(b, sortOrder));
                }

                blogs = blogs.Skip((pageNumber - 1) * pageSize).Take(pageSize);

                return blogs.AsNoTracking().ToList();
            }
        }

        public int Count()
        {
            using (context = new BloggingContext())
            {
                return this.context.Blogs.Count();
            }
        }
    }
}