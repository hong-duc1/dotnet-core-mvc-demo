using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using Microsoft.Extensions.Logging;
using mvc_postgresql_demo.DAO;
using mvc_postgresql_demo.Models;
using mvc_postgresql_demo.Controllers;

namespace mvc_postgresql_demo_test
{
    public class BlogControllerTest
    {
        [Fact]
        public void Index_ReturnAViewResult_WithAListOfBlog()
        {
            var mockRepo = new Mock<IDAO<Blog>>();
            mockRepo.Setup(repo => repo.GetMany(null)).Returns(GetListBlog());
            var mockLog = new Mock<ILogger<BlogController>>();
            var controller = new BlogController(null, mockLog.Object, mockRepo.Object);

            var result = controller.Index();

            var viewResult = Assert.IsType<ViewResult>(result);
            var model = Assert.IsAssignableFrom<IEnumerable<Blog>>(viewResult.ViewData.Model);
            Assert.Equal(2, model.Count());
        }

        private List<Blog> GetListBlog()
        {
            var blogs = new List<Blog>();
            blogs.Add(new Blog()
            {
                BlogId = 1,
                Url = "https://www.reddit.com/r/programming/",
                Posts = new List<Post>()
            });
            blogs.Add(new Blog()
            {
                BlogId = 2,
                Url = "https://www.reddit.com/r/programming/",
                Posts = new List<Post>()
            });
            return blogs;
        }
    }
}
